package com.andware.JsonDeal;

import com.google.gson.Gson;

public class JsonHandle {

	public static <T> T JsonToObj ( String json , Class<T> cls ) {
		T t;
		Gson gson = new Gson();
		t = gson.fromJson(json, cls);
		return t;
	}
	
	public static <T> String ObjToJson ( T obj ) {
		Gson gson = new Gson();
		return gson.toJson(obj);
	}
	
}
