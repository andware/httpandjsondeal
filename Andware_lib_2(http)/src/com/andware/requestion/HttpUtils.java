package com.andware.requestion;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.preference.PreferenceActivity.Header;
import android.util.Log;
import android.webkit.CookieManager;

public class HttpUtils {

	// post 请求
	public static synchronized String SendPostRequest(String adress_Http, String strJson ) {

		URL url = null;
		try {
			url = new URL(adress_Http);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpPost httpPost = new HttpPost(url.toString());

		StringEntity entity = null;
		try {
			entity = new StringEntity(strJson,"utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		entity.setContentType("application/json;charset=utf-8");
		entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
				"application/json;charset=utf-8"));
		// Send request to WCF service
		DefaultHttpClient httpClient = new DefaultHttpClient();

		HttpResponse response = null;
		httpPost.setEntity(entity);

		try {
			response = httpClient.execute(httpPost);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpEntity entity1 = response.getEntity();

		String responseString = null;
		if (entity1 != null
				&& (response.getStatusLine().getStatusCode() == 201 || response
				.getStatusLine().getStatusCode() == 200)) {
			// --just so that you can view the response, this is optional--
			try {
				responseString = EntityUtils.toString(entity1);
				Log.v("rsult----->", responseString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			// int sc = response.getStatusLine().getStatusCode();
			// String sl = response.getStatusLine().getReasonPhrase();
			try {
				responseString = EntityUtils.toString(entity1);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.v("rsult----->", responseString);
		}
		return responseString;

	}

	// get
	public static String GetRequest(String adress_Http,
			String... requestContent) {
		String resultData = "";
		InputStreamReader in = null;
		HttpURLConnection urlConn = null;
		BufferedReader buffer = null;
		try {
			StringBuffer sBuffer = new StringBuffer();
			for (int n = 0; n < requestContent.length; n++) {
				if ((n + 1) % 2 == 0) {
					sBuffer.append("=" + requestContent[n] + "&");
				} else {
					sBuffer.append(requestContent[n]);
				}
			}
			URL url;
			if (sBuffer.length() == 0) {
				url = new URL(adress_Http);
			} else {
				url = new URL(adress_Http + "GET" + "?" + sBuffer);
			}

			if (url != null) {
				urlConn = (HttpURLConnection) url.openConnection();
				urlConn.setConnectTimeout(3000);

				try {
					in = new InputStreamReader(urlConn.getInputStream());
				} catch (ConnectException e) {
					return resultData;
				}
				buffer = new BufferedReader(in);
				String inputLine = null;

				while ((inputLine = buffer.readLine()) != null) {
					resultData += inputLine + "\n";
				}
				urlConn.disconnect();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (buffer != null) {
					buffer.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return resultData;
	}

	// put
	public static synchronized String HttpPutRequest(String api_url, String putJson) {
		// --This code works for updating a record from the feed--

		URL url = null;
		try {
			url = new URL(api_url);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpPut httpPut = new HttpPut(url.toString());

		StringEntity entity = null;
		try {
			entity = new StringEntity(putJson,"utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		entity.setContentType("application/json;charset=utf-8");
		entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
				"application/json;charset=utf-8"));
		httpPut.setEntity(entity);

		// Send request to WCF service
		DefaultHttpClient httpClient = new DefaultHttpClient();

		HttpResponse response = null;
		try {
			response = httpClient.execute(httpPut);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpEntity entity1 = response.getEntity();

		String responseString = null;
		if (entity1 != null
				&& (response.getStatusLine().getStatusCode() == 201 || response
				.getStatusLine().getStatusCode() == 200)) {
			// --just so that you can view the response, this is optional--
			try {
				responseString = EntityUtils.toString(entity1);
				Log.v("rsult----->", responseString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			// int sc = response.getStatusLine().getStatusCode();
			// String sl = response.getStatusLine().getReasonPhrase();
			try {
				responseString = EntityUtils.toString(entity1);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.v("rsult----->", responseString);
		}
		return responseString;
	}

	public static synchronized String httpDeleteRequest ( String api_url ) {
		String responseString = null;

		HttpClient httpClient = new DefaultHttpClient();  
		HttpConnectionParams  
		.setConnectionTimeout(httpClient.getParams(), 6000);  
		HttpResponse response = null;  
		try {
			response = httpClient.execute(new HttpDelete(api_url));
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		HttpEntity entity = response.getEntity();
		if (entity != null
				&& (response.getStatusLine().getStatusCode() == 201 || response
				.getStatusLine().getStatusCode() == 200)) {
			// --just so that you can view the response, this is optional--
			try {
				responseString = EntityUtils.toString(response.getEntity());
				Log.v("rsult----->", responseString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			// int sc = response.getStatusLine().getStatusCode();
			// String sl = response.getStatusLine().getReasonPhrase();
			try {
				responseString = EntityUtils.toString(response.getEntity());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.v("rsult----->", responseString);
		}
		return responseString;

	}

}
