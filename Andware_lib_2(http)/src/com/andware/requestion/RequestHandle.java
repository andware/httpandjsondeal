package com.andware.requestion;

import android.util.Log;

import com.andware.JsonDeal.JsonHandle;
/**
 * @author ssj
 * 
 * Http Json数据请求处理封装，整个封装基于泛型
 * 分装有如下请求方式：
 * 1.get
 * 2.post
 * 3.put
 * 
 * **/
import com.google.gson.Gson;

public class RequestHandle {
	
	/**
	 * @author ssj
	 * 
	 * Get请求
	 * 
	 * @param urlString 请求的网络地址
	 * @param cls 需要将请求的转换为对象的泛型
	 * @param requestContent 请求内容
	 * 
	 * @return 泛型对象请求结果
	 * **/
	
	public static <T> T getObjectForRequest (String urlString,Class<T> cls,String... requestContent) {
		T t;
		String json = HttpUtils.GetRequest(urlString, requestContent);
		Log.i("User",json);
        t = JsonHandle.JsonToObj(json, cls);
		return t;
	}
	
	/**
	 * @author ssj
	 * 
	 * Delete请求
	 * 
	 * @param urlString 请求的网络地址
	 * @param cls 需要将请求的转换为对象的泛型
	 * @param requestContent 请求内容
	 * 
	 * @return 泛型对象请求结果
	 * **/
	
	public static <T> T deleteObjectForRequest (String urlString,Class<T> cls,String... requestContent) {
		T t;
		String json = HttpUtils.httpDeleteRequest(urlString);
        t = JsonHandle.JsonToObj(json, cls);
		return t;
	}
	
	/**
	 * @author ssj
	 * 
	 * Post请求
	 * @param <E>
	 * 
	 * @param urlString 请求的网络地址
	 * @param postObject 需要post的数据对象
	 * @param requestCls 需要将请求的Json数据转换为对象的泛型
	 * 
	 * 
	 * @return 泛型对象请求结果
	 * **/
	
	public static <T, E> E postObjectForRequest ( String urlString , T postObject , Class<E> requestCls ) {
		E e;
		String sendJson = JsonHandle.ObjToJson(postObject);
		Log.i("PostJson",sendJson);
		String json = HttpUtils.SendPostRequest(urlString, sendJson);
		Log.i("PostJson","get:"+json);
		e = JsonHandle.JsonToObj(json, requestCls);
		return e;
	}
	
	/**
	 * @author ssj
	 * 
	 * Put请求
	 * @param <T>
	 * 
	 * @param urlString 请求的网络地址
	 * @param putObject 需要put的数据对象
	 * @param cls 需要将请求的转换为对象的泛型
	 * 
	 * @return 泛型对象请求结果
	 * **/
	
	public static <E, T> E putObjectForRequest ( String urlString , T putObject , Class<E> requestCls ) {
		E e;
		String json = JsonHandle.ObjToJson(putObject);
		e = JsonHandle.JsonToObj(HttpUtils.HttpPutRequest(urlString, json),requestCls);
        return e;
	}
	
}
